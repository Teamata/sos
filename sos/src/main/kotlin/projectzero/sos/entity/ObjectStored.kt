package projectzero.sos.entity

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*

@Entity
data class ObjectStored (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var objectId: Long = 0L,
        var name: String = "", //basically file name
        var eTag: String = "",
        var created: Long = 0L,
        var modified: Long = 0L,
        var uploadTicket: Boolean = false,
        var totalSize: Long = 0L,

        @ManyToOne
        @JoinColumn(name = "bucketId")
        @JsonBackReference
        var bucket: Bucket = Bucket(),

        @OneToMany(mappedBy = "objectStored", cascade = arrayOf(CascadeType.ALL))
        @JsonManagedReference
        var metaData : MutableList<Metadata> = mutableListOf(),

        @OneToMany(mappedBy = "objectStored", cascade = arrayOf(CascadeType.ALL))
        @JsonManagedReference
        var parts: MutableList<ObjectPart> = mutableListOf()

)