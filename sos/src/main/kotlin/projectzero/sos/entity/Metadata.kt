package projectzero.sos.entity

import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.*

@Entity
data class Metadata (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var metaId: Long = 0L,
        var metaKey: String = "",
        var metaValue: String = "",

        @ManyToOne
        @JoinColumn(name = "objectId")
        @JsonBackReference
        var objectStored: ObjectStored = ObjectStored()
)