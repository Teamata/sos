package projectzero.sos.entity

import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*


@Entity
data class Bucket (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var bucketId: Long = 0L,
    var bucketName:String = "" , //folder name
    var created: Long = 0L,
    var modified: Long = 0L,

    @OneToMany(mappedBy = "bucket", cascade = arrayOf(CascadeType.ALL))
    @JsonManagedReference
    var objectsStrored : MutableList<ObjectStored> = mutableListOf()


)