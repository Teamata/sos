package projectzero.sos.entity

import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.*

@Entity
class ObjectPart (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id : Long = 0L,
        var name: String = "",
        var part: Long = 0L,
        var size: Long = 0L,
        var startByte: Long? = null,
        var endByte: Long? = null,
        var md5: String = "",

        @ManyToOne
        @JoinColumn(name = "objectId")
        @JsonBackReference
        var objectStored: ObjectStored = ObjectStored()

)