package projectzero.sos.controller

import org.springframework.context.annotation.Bean
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import projectzero.sos.response.Response


@RestController
@RequestMapping("/test")
class HelloController{

    @RequestMapping(value = ["hello"] ,method = arrayOf(RequestMethod.GET))
    fun hello(): ResponseEntity<Response> {
        return ResponseEntity.ok(Response(status="ok it's working!",error = "none"))
    }
}