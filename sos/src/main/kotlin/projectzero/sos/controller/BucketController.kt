package projectzero.sos.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import projectzero.sos.dto.BucketDto
import projectzero.sos.dto.CreatedBucketDto
import projectzero.sos.error.BucketError
import projectzero.sos.repository.BucketRepository
import projectzero.sos.response.Response
import projectzero.sos.service.BucketService
import projectzero.sos.validation.BucketValidation


@RestController
@RequestMapping("{bucketName}")
class BucketController {

    @Autowired
    lateinit var bucketValidation: BucketValidation

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Autowired
    lateinit var bucketService: BucketService

    @RequestMapping(method = arrayOf(RequestMethod.POST), params = ["create"])
    fun createBucket(
            @PathVariable("bucketName") bucketName: String
    ): ResponseEntity<Any>{
        if (bucketValidation.checkCreate(bucketName) != null){
            return ResponseEntity.badRequest().body(Response(error = BucketError.ALREADYEXIST.value, status = "error"))
        }
        else if (!bucketValidation.checkProperName(bucketName)) {
            return ResponseEntity.badRequest().body(Response(error = BucketError.INVALIDNAME.value, status = "error"))
        }
        else{
            var bucket = bucketService.create(bucketName)
            return ResponseEntity.ok(CreatedBucketDto(bucket))
        }
    }

    @RequestMapping(method = arrayOf(RequestMethod.DELETE), params = ["delete"])
    fun deleteBucket(
            @PathVariable("bucketName") bucketName: String
    ): ResponseEntity<Response>{
        if (bucketValidation.checkCreate(bucketName) != null) {
            bucketService.deleteBucket(bucketName)
            return ResponseEntity.ok(Response(error = "none", status = "success" ))
        }
        return ResponseEntity.badRequest().body(Response(error = BucketError.NOTFOUND.value, status = "error"))
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET), params = ["list"])
    fun listBucket(
            @PathVariable("bucketName") bucketName: String
    ): ResponseEntity<Any> {
        if (bucketValidation.checkCreate(bucketName) != null) {
            //should call repository in service
            return ResponseEntity.ok(BucketDto(bucketService.getBucket(bucketName)!!))
        }
        return ResponseEntity.badRequest().body(Response(error = BucketError.NOTFOUND.value, status = "error"))
    }

}