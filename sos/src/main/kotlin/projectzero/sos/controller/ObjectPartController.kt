package projectzero.sos.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import projectzero.sos.dto.ObjectStoredUploadedDto
import projectzero.sos.error.BucketError
import projectzero.sos.error.ObjectPartError
import projectzero.sos.error.ObjectStoredError
import projectzero.sos.response.Response
import projectzero.sos.service.ObjectPartService
import projectzero.sos.service.ObjectStoredService
import projectzero.sos.validation.BucketValidation
import projectzero.sos.validation.ObjectPartValidation
import projectzero.sos.validation.ObjectStoredValidation
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("{bucketName}/{objectName}")
class ObjectPartController {

    @Autowired
    lateinit var objectPartValidation: ObjectPartValidation

    @Autowired
    lateinit var objectPartService: ObjectPartService

    @Autowired
    lateinit var objectStoredValidation: ObjectStoredValidation

    @Autowired
    lateinit var bucketValidation: BucketValidation

    @Autowired
    lateinit var objectStoredService: ObjectStoredService

    @RequestMapping(method = arrayOf(RequestMethod.PUT))
    fun uploadParts(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            @RequestParam("partNumber") partNumber: Long,
            request: HttpServletRequest
    ): ResponseEntity<Any> {
        var expectedMd5: String = request.getHeader("content-md5")
        var expectedLength: String = request.getHeader("content-length")
        var objectStoredUploadedDto: ObjectStoredUploadedDto = objectPartValidation.validUpload(
                objectName = objectName,
                bucketName = bucketName,
                partNumber = partNumber,
                expectedLength = expectedLength,
                expectedMd5 = expectedMd5)
        if(objectStoredUploadedDto.error.isNotEmpty()) {
            return ResponseEntity.badRequest().body(objectStoredUploadedDto)
        }
        if(!objectStoredValidation.checkProperName(objectName)){
            objectStoredUploadedDto.error = ObjectStoredError.INVALIDNAME.value
            return ResponseEntity.badRequest().body(objectStoredUploadedDto)
        }
        var createSuccess: Boolean = objectPartService.createFile(request = request,objectStoredUploadedDto = objectStoredUploadedDto,
                bucketName = bucketName, objectName = objectName, partNumber = partNumber, md5 = expectedMd5)
        if(createSuccess && objectStoredService.isTicketed(objectName, bucketName)){
            objectPartService.savePart(
                    objectName = objectName,
                    part = partNumber,
                    md5 = expectedMd5,
                    size = expectedLength.toLong(),
                    bucketName = bucketName
            )
            return ResponseEntity.ok().body(objectStoredUploadedDto)
        }
        return ResponseEntity.badRequest().body(objectStoredUploadedDto)
    }

    @RequestMapping(method = arrayOf(RequestMethod.DELETE))
    fun delete(@PathVariable("bucketName") bucketName: String,
               @PathVariable("objectName") objectName: String,
               @RequestParam("partNumber") partNumber: Long): ResponseEntity<Response>{
        if(bucketValidation.checkCreate(bucketName) == null){
            return ResponseEntity.badRequest().body(Response(status = "error", error = BucketError.NOTFOUND.value))
        }
        if(!objectStoredService.isExist(objectName = objectName, bucketName = bucketName)){
            return ResponseEntity.badRequest().body(Response(status = "error", error = ObjectStoredError.NOTFOUND.value))
        }
        if(!objectStoredService.isTicketed(bucketName = bucketName, objectName = objectName)){
            return ResponseEntity.badRequest().body(Response(status = "error", error = ObjectStoredError.INVALIDUPLOADTICKET.value))
        }
        if(objectPartService.getPart(objectName = objectName, bucketName = bucketName, part = partNumber) == null){
            return ResponseEntity.badRequest().body(Response(status = "error", error = ObjectPartError.NOTFOUND.value))
        }
        objectPartService.deletePart(objectName = objectName, bucketName = bucketName, part = partNumber)
        return ResponseEntity.ok(Response(status = "success", error = "none"))
    }
}