package projectzero.sos.controller

import org.apache.commons.codec.digest.DigestUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import javax.servlet.http.HttpServletRequest
import org.springframework.web.bind.annotation.*
import projectzero.sos.dto.ObjectCompleteDto
import projectzero.sos.dto.ObjectStoredUploadedDto
import projectzero.sos.error.BucketError
import projectzero.sos.error.ObjectStoredError
import projectzero.sos.error.RequestError
import projectzero.sos.response.Response
import projectzero.sos.service.BucketService
import projectzero.sos.service.ObjectPartService
import projectzero.sos.service.ObjectStoredService
import projectzero.sos.validation.BucketValidation
import projectzero.sos.validation.ObjectPartValidation
import projectzero.sos.validation.ObjectStoredValidation
import javax.servlet.http.HttpServletResponse


@RestController
@RequestMapping("{bucketName}/{objectName}")
class ObjectStoredController {

    @Autowired
    lateinit var objectPartValidation: ObjectPartValidation

    @Autowired
    lateinit var objectPartService: ObjectPartService

    @Autowired
    lateinit var objectStoredValidation: ObjectStoredValidation

    @Autowired
    lateinit var bucketValidation: BucketValidation

    @Autowired
    lateinit var objectStoredService: ObjectStoredService

    //basically check if the file already exist and bucket must exist.
    @RequestMapping(method = arrayOf(RequestMethod.POST), params = ["create"])
    fun createTicket(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            request: HttpServletRequest
    ): ResponseEntity<Response>{
        if(bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.badRequest().body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if (objectStoredService.isExist(objectName,bucketName)) {
            return ResponseEntity.badRequest().body(Response(error = ObjectStoredError.ALREADYEXIST.value, status = "error"))
        }
        if(!objectStoredValidation.checkProperName(objectName)){
            return ResponseEntity.badRequest().body(Response(error = ObjectStoredError.INVALIDNAME.value, status = "error"))
        }
        objectStoredService.createTicket(bucketName,objectName)
        return ResponseEntity.ok(Response(error = "none", status = "success"))
    }

    //request to the server that the upload is finished.
    @RequestMapping(method = arrayOf(RequestMethod.POST), params = ["complete"])
    fun completeUpload(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            request: HttpServletRequest
    ): ResponseEntity<ObjectCompleteDto>{
        var objectCompleteDto = ObjectCompleteDto()
        if(bucketValidation.checkCreate(bucketName) == null){
            objectCompleteDto.error = BucketError.NOTFOUND.value
            return ResponseEntity.badRequest().body(objectCompleteDto)
        }
        if(!objectStoredService.isExist(objectName = objectName, bucketName = bucketName)){
            objectCompleteDto.error = ObjectStoredError.NOTFOUND.value
            return ResponseEntity.badRequest().body(objectCompleteDto)
        }
        if(!objectStoredService.isTicketed(bucketName = bucketName, objectName = objectName)){
            objectCompleteDto.error = ObjectStoredError.INVALIDUPLOADTICKET.value
            return ResponseEntity.badRequest().body(objectCompleteDto)
        }
        objectStoredService.complete(bucketName = bucketName,objectName = objectName, objectCompleteDto = objectCompleteDto)
        return ResponseEntity.ok(objectCompleteDto)
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun download(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            request: HttpServletRequest,
            response: HttpServletResponse): ResponseEntity<Any>{
        var range = request.getHeader("Range")
        if(range==null){
            range = "Bytes=0-"
        }
        if(!objectStoredValidation.checkRange(range) && range.isNotEmpty()){
            return ResponseEntity.badRequest().body(Response(error = RequestError.INVALIDRANGE.value,status = "error"))
        }
        if(bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.badRequest().body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if(!objectStoredService.isExist(objectName,bucketName)) {
            return ResponseEntity.badRequest().body(Response(error = ObjectStoredError.NOTFOUND.value, status = "error"))
        }
        if(objectStoredService.isTicketed(bucketName = bucketName, objectName = objectName)){
            return ResponseEntity.badRequest().body(Response(error = ObjectStoredError.INVALIDUPLOADTICKET.value, status = "error"))
        }
        //try to download
        var res = objectStoredService.download(bucketName = bucketName, objectName = objectName, response = response, range = range)
        if(!res.error.equals("none")){
            return ResponseEntity.badRequest().body(res)
        }
        return ResponseEntity.ok().build()
    }

    @RequestMapping(value = ["/check"], method = arrayOf(RequestMethod.GET))
    fun checkObjectExist(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String
    ):ResponseEntity<Response> {
        if (bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.badRequest().body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if (!objectStoredService.isExist(objectName, bucketName)) {
            return ResponseEntity.badRequest().body(Response(error = ObjectStoredError.NOTFOUND.value, status = "error"))
        }
        return ResponseEntity.ok(Response(status = "success", error="none"))
    }

    @RequestMapping(method = arrayOf(RequestMethod.DELETE), params = ["delete"])
    fun delete(@PathVariable("bucketName") bucketName: String,
               @PathVariable("objectName") objectName: String): ResponseEntity<Response> {
        if(bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.badRequest().body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if (!objectStoredService.isExist(objectName,bucketName)) {
            return ResponseEntity.badRequest().body(Response(error = ObjectStoredError.NOTFOUND.value, status = "error"))
        }
        objectStoredService.delete(bucketName = bucketName, objectName = objectName)
        return ResponseEntity.ok(Response(status = "success", error = "none"))
    }

    @RequestMapping(method = arrayOf(RequestMethod.PUT), params = ["metadata"])
    fun updateMetadata(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            @RequestParam("key") key: String,
            request: HttpServletRequest
    ): ResponseEntity<Response> {
        if(bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if (!objectStoredService.isExist(objectName,bucketName)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = ObjectStoredError.NOTFOUND.value, status = "error"))
        }
        objectStoredService.updateMetadata(bucketName, objectName, request, key)
        return ResponseEntity.ok(Response(status = "success", error = "none"))
    }

    @RequestMapping(method = arrayOf(RequestMethod.DELETE), params = ["metadata"])
    fun deleteMetadata(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            @RequestParam("key") key: String,
            request: HttpServletRequest): ResponseEntity<Response> {
        if(bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if (!objectStoredService.isExist(objectName,bucketName)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = ObjectStoredError.NOTFOUND.value, status = "error"))
        }
        objectStoredService.deleteMetadata(bucketName, objectName, key)
        return ResponseEntity.ok(Response(status = "success", error = "none"))
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET), params = ["metadata", "key"])
    fun getMetadata(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            @RequestParam("key") key: String,
            response: HttpServletResponse): ResponseEntity<Response> {
        if(bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if (!objectStoredService.isExist(objectName,bucketName)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = ObjectStoredError.NOTFOUND.value, status = "error"))
        }
        objectStoredService.getMetadata(bucketName, objectName, key, response)
        return ResponseEntity.ok().build()
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET), params = ["metadata"])
    fun getAllMetadata(
            @PathVariable("bucketName") bucketName: String,
            @PathVariable("objectName") objectName: String,
            response: HttpServletResponse): ResponseEntity<Response> {
        if(bucketValidation.checkCreate(bucketName) == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = BucketError.NOTFOUND.value, status = "error"))
        }
        if (!objectStoredService.isExist(objectName,bucketName)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response(error = ObjectStoredError.NOTFOUND.value, status = "error"))
        }
        objectStoredService.getAllMetadata(bucketName, objectName, response)
        return ResponseEntity.ok().build()
    }

}