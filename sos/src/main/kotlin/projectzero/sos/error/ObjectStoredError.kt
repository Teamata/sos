package projectzero.sos.error

enum class ObjectStoredError(val value: String) {
    INVALIDNAME("InvalidName"),
    INVALIDUPLOADTICKET("UploadTicketNotFound"),
    ALREADYEXIST("ObjectAlreadyExist"),
    NOTFOUND("InvalidObject")
}