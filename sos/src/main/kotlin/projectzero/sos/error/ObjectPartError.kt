package projectzero.sos.error

enum class ObjectPartError(val value: String) {
    INVALIDNUMBER("InvalidPartNumber"),
    MD5MISMATCH("MD5Mismatched"),
    NOTFOUND("PartNotFound")
}