package projectzero.sos.error

enum class RequestError(val value: String) {
    INVALIDCONTENTLENGTH("ContentMismatch"),
    INVALIDRANGE("InvalidRange")
}