package projectzero.sos.error

enum class BucketError(val value: String) {
    INVALIDNAME("InvalidName"),
    ALREADYEXIST("BucketAlreadyExists"),
    NOTFOUND("InvalidBucket")
}