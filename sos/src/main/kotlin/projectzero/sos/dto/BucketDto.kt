package projectzero.sos.dto

import projectzero.sos.entity.Bucket

data class BucketDto (
        var name:String = "",
        var created: Long = 0L,
        var modified: Long = 0L,
        var objects: MutableList<ObjectStroedDto> = mutableListOf()

){
    constructor(bucket: Bucket): this() {
        name = bucket.bucketName
        created = bucket.created
        modified = bucket.modified
        for(o in bucket.objectsStrored){
            objects.add(ObjectStroedDto(o))
        }
    }
}