package projectzero.sos.dto

import projectzero.sos.entity.ObjectStored

data class ObjectCompleteDto (
        var eTag: String = "",
        var length: Long = 0L,
        var name: String = "",
        var error: String = ""
){
    constructor(objectStored: ObjectStored, error: String): this(){
        eTag = objectStored.eTag
        length = objectStored.totalSize
        name = objectStored.name
        this.error = error
    }
}