package projectzero.sos.dto


data class ObjectStoredUploadedDto (
        var md5: String = "",
        var length: Long = 0L,
        var partNumber: Long = 0L,
        var error: String = ""
)