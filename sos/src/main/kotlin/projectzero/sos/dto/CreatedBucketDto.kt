package projectzero.sos.dto

import projectzero.sos.entity.Bucket


data class CreatedBucketDto (
        var name:String = "", //folder name
        var created: Long = 0L,
        var modified: Long = 0L
) {
    constructor(bucket: Bucket): this() {
        name = bucket.bucketName
        created = bucket.created
        modified = bucket.created
    }
}