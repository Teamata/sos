package projectzero.sos.dto

import projectzero.sos.entity.ObjectStored

data class ObjectStroedDto(
        var name: String = "",
        var eTag: String = "",
        var created: Long = 0L,
        var modified: Long = 0L,
        var size:Long = 0L
) {
    constructor(obj: ObjectStored): this(){
        name = obj.name
        created = obj.created
        modified = obj.modified
        eTag = obj.eTag
        size = obj.totalSize
    }
}