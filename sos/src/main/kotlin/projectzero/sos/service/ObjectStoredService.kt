package projectzero.sos.service

import org.apache.commons.io.IOUtils
import org.apache.commons.io.input.BoundedInputStream
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.codec.Hex
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.DigestUtils
import projectzero.sos.dto.ObjectCompleteDto
import projectzero.sos.entity.Bucket
import projectzero.sos.entity.Metadata
import projectzero.sos.entity.ObjectPart
import projectzero.sos.entity.ObjectStored
import projectzero.sos.error.RequestError
import projectzero.sos.repository.BucketRepository
import projectzero.sos.repository.MetadataRepository
import projectzero.sos.repository.ObjectPartRepository
import projectzero.sos.repository.ObjectStoredRepository
import projectzero.sos.response.Response
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.io.SequenceInputStream
import java.time.Instant
import java.util.*
import org.json.JSONObject
import javax.activation.MimetypesFileTypeMap
import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Service
class ObjectStoredService {

    private final var RANGE = Pattern.compile("(-?\\d*)-(-?\\d*)")

    @Autowired
    lateinit var objectStoredRepository: ObjectStoredRepository

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Autowired
    lateinit var objectPartRepository: ObjectPartRepository

    @Autowired
    lateinit var metadataRepository: MetadataRepository

    @Transactional
    fun createTicket(bucketName: String, objectName: String): Boolean {
        var bucket: Bucket = bucketRepository.findByBucketName(bucketName)!!
        var objectStored = ObjectStored(
                name = objectName,
                eTag = "",
                created = Date.from(Instant.now()).time,
                modified = Date.from(Instant.now()).time,
                uploadTicket = true,
                totalSize = 0,
                bucket = bucket,
                metaData = mutableListOf(),
                parts = mutableListOf()
        )
        objectStoredRepository.save(objectStored)
        return true
    }

    @Transactional
    fun getObjectStored(objectName: String, bucketName: String): ObjectStored? {
        var bucket : Bucket = bucketRepository.findByBucketName(bucketName)!!
        return objectStoredRepository.findByNameAndBucket(objectName, bucket)
    }

    @Transactional
    fun isTicketed(objectName: String, bucketName: String): Boolean {
        var bucket : Bucket? = bucketRepository.findByBucketName(bucketName)
        if(bucket == null){
            return false
        }
        var obj: ObjectStored? = objectStoredRepository.findByNameAndBucket(objectName, bucket)
        if(obj == null){
            return false
        }
        return obj.uploadTicket
    }

    @Transactional
    fun isExist(objectName: String, bucketName: String): Boolean {
        var bucket : Bucket = bucketRepository.findByBucketName(bucketName)!!
        var objStored = objectStoredRepository.findByNameAndBucket(objectName,bucket)
        return objStored != null
    }

    //bucket and obj exist and valid
    @Transactional
    fun complete(objectName: String, bucketName: String, objectCompleteDto: ObjectCompleteDto): Boolean {
        var buck = bucketRepository.findByBucketName(bucketName)!!
        var obj = objectStoredRepository.findByNameAndBucket(objectName, buck)!!
        var parts: MutableList<ObjectPart> = obj.parts
        parts = parts.sortedBy{it.part}.toMutableList()
        var partNum = 0
        var cumulatedBytes: Long = 0L
        var cumulatedMd5: String = ""
        for(p in parts){
            p.startByte = cumulatedBytes
            cumulatedBytes += p.size - 1
            p.endByte = cumulatedBytes
            objectPartRepository.save(p)
            cumulatedBytes += 1
            cumulatedMd5 += p.md5
            partNum += 1
        }
        var hex = DigestUtils.md5DigestAsHex(Hex.decode(cumulatedMd5))
        obj.eTag = hex+"-"+partNum
        obj.totalSize = cumulatedBytes
        obj.uploadTicket = false
        objectStoredRepository.save(obj)
        objectCompleteDto.eTag = obj.eTag
        objectCompleteDto.error = ""
        objectCompleteDto.length = obj.totalSize
        objectCompleteDto.name = obj.name

        //add mime type
        var mime = MimetypesFileTypeMap().getContentType(obj.name)
        var metadata: Metadata = Metadata(metaKey = "Content-Type", metaValue = mime, objectStored = obj)
        metadataRepository.save(metadata)
        return true
    }

    @Transactional
    fun delete(bucketName: String, objectName: String): Boolean {
        var obj = getObjectStored(bucketName = bucketName, objectName = objectName)!!
        objectStoredRepository.delete(obj)
        //should delete all parts but later. Let them live locally for now :)
        return true
    }

    /**
     * SHITTY WAY OF DOING DOWNLOAD, IT CAN BE IMPROVE BUT IT'S 3 AM AND MY BRAIN WON'T FUNCTION
     */

    @Transactional
    fun download(bucketName: String, objectName: String, range: String, response: HttpServletResponse): Response {
        var start = 0L
        var end = -1L
        var obj = getObjectStored(objectName = objectName, bucketName = bucketName)!!
        var strip = RANGE.matcher(range)
        var str = ""
        while(strip.find()){
//            println("stripped  ${strip.group()}")
            str = strip.group()
        }
        if(str.isEmpty()){
            end = obj.totalSize-1
        }
        else {
            var stripRange = str.split('-')
            start = stripRange[0].toLong()
            if(stripRange[1].isEmpty()){
                end = obj.totalSize-1
            }
            else{
                end = stripRange[1].toLong()
            }
        }
        if(end >= obj.totalSize || start > obj.totalSize){
            return Response(status = "error", error = RequestError.INVALIDRANGE.value)
        }
//        println("start : $start , end : $end")
//        var tmp = end-start
//        println("total size : $tmp")
        var files = objectPartRepository.findByNameAndObjectStoredAndStartByteLessThanEqualAndEndByteGreaterThanEqual(
                name = objectName, objectStored = obj,startByte = end, endByte = start)
        files = files.sortedBy{it.part}.toMutableList()
        var isStartfile: Boolean = true
        var neededData
                : MutableList<InputStream> = mutableListOf()
        for(f in files){
            var tempFile: File = File(objectName)
            var extension = tempFile.extension
            var name = tempFile.nameWithoutExtension
            var readData = FileInputStream("../buckets/$bucketName/$name-${f.part}.$extension")
//            println("file ${f.startByte} and ${f.endByte}")
            if(isStartfile){
                //skip up to the desired starting point
                readData.skip(start-f.startByte!!)
                //if the end point is within this file
                if(f.endByte!! > end){
                    var boundInputStream = BoundedInputStream(readData,end-start+1)
                    neededData.add(boundInputStream)
//                    println("size of this ${end-start}")
//                    println("start: $start, end : $end")
                }
                else{
                    neededData.add(readData)
//                    println("start: $start, end : ${f.endByte}")
                }
                isStartfile = false

            }
            else if(f.startByte!! <= end && f.endByte!! <= end && !isStartfile){
//                println("second if")
                neededData.add(readData)
//                println("start: ${f.startByte}, end : ${f.endByte}")
            }
            else if(f.startByte!! <= end && f.endByte!! > end){
//                println("third if")
                var boundInputStream = BoundedInputStream(readData,end-f.startByte!!+1)
                neededData.add(boundInputStream)
//                println("size of this ${end-f.startByte!!}")
//                println("start: ${f.startByte}, end : $end")
            }
            else{
//                println("start: ${f.startByte}, end : ${f.endByte}")
            }
        }
        response.setHeader("eTag",obj.eTag)
        var content = ""
        for (key in obj.metaData){
            if(key.metaKey == "Content-Type"){
                content = key.metaValue
            }
        }
        response.setHeader("Content-Type",content)
        IOUtils.copyLarge(SequenceInputStream(Collections.enumeration(neededData)),response.outputStream)
        return Response(status = "success", error = "none")
    }

    @Transactional
    fun updateMetadata(bucketName: String, objectName: String, request : HttpServletRequest, key : String): Boolean{
        var obj = getObjectStored(objectName = objectName, bucketName = bucketName)!!
        var data = IOUtils.toString(request.inputStream)
        var found = false
        for(m in obj.metaData){
            if(m.metaKey == key){
                m.metaValue = data
                metadataRepository.save(m)
                found = true
            }
        }
        if(!found){
            var metadata: Metadata = Metadata(metaKey = key, metaValue = data, objectStored = obj)
            metadataRepository.save(metadata)
        }
        return true
    }

    @Transactional
    fun getMetadata(bucketName: String, objectName: String, key : String, response: HttpServletResponse): Boolean {
        var obj = getObjectStored(objectName = objectName, bucketName = bucketName)!!
        var data = Metadata()
        response.contentType = "application/json"
        response.characterEncoding = "utf-8"
        val out = response.writer
        val json: JSONObject = JSONObject()
        for(m in obj.metaData){
            if(m.metaKey == key){
                data = m
                json.put(data.metaKey,data.metaValue)
            }
        }
        out.write(json.toString())
        return true
    }

    @Transactional
    fun deleteMetadata(bucketName: String, objectName: String, key : String): Boolean {
        var obj = getObjectStored(objectName = objectName, bucketName = bucketName)!!
        var found = false
        var data = Metadata()
        for(m in obj.metaData){
            if(m.metaKey == key){
                data = m
                found = true
            }
        }
        if(found){
            obj.metaData.remove(data)
            return true
        }
        return false
    }

    @Transactional
    fun getAllMetadata(bucketName: String, objectName: String, response: HttpServletResponse): Boolean {
        var obj = getObjectStored(objectName = objectName, bucketName = bucketName)!!
        response.contentType = "application/json"
        response.characterEncoding = "utf-8"
        val out = response.writer
        val json: JSONObject = JSONObject()
        for(m in obj.metaData){
            json.put(m.metaKey,m.metaValue)
        }
        out.write(json.toString())
        return false
    }

}