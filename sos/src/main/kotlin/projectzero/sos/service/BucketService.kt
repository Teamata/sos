package projectzero.sos.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import projectzero.sos.entity.Bucket
import projectzero.sos.entity.ObjectStored
import projectzero.sos.repository.BucketRepository
import projectzero.sos.repository.ObjectStoredRepository
import java.io.File
import java.time.Instant
import java.util.*

@Service
class BucketService {

    @Autowired
    private lateinit var bucketRepository: BucketRepository

    @Autowired
    private lateinit var objectStoredRepository: ObjectStoredRepository

    @Transactional
    fun create(bucketName : String): Bucket{
        var bucket: Bucket = Bucket(bucketName = bucketName,created = Date.from(Instant.now()).time, modified = Date.from(Instant.now()).time, objectsStrored = mutableListOf())
        bucketRepository.save(bucket)
        File("../buckets/$bucketName").mkdirs()
        return bucket
    }

    @Transactional
    fun deleteBucket(bucketName : String): Boolean {
        var bucket: Bucket? = bucketRepository.findByBucketName(bucketName)
        if(bucket != null) {
            var bucketDir = File("../buckets/$bucketName")
            bucketDir.deleteRecursively()
            bucketDir.delete()
            bucketRepository.delete(bucket)
            return true
        }
        return false
    }

    fun mock(bucket: Bucket) {
        var obj1 = ObjectStored(name = "ggg.py", eTag = "eTag", created = Date.from(Instant.now()).time,
                modified = Date.from(Instant.now()).time, bucket = bucket)
        objectStoredRepository.save(obj1)
        bucket.objectsStrored.add(obj1)
        var obj2 = ObjectStored(name = "app.py", eTag = "eTag", created = Date.from(Instant.now()).time,
                modified = Date.from(Instant.now()).time, bucket = bucket)
        objectStoredRepository.save(obj2)
        bucket.objectsStrored.add(obj2)
        var obj3 = ObjectStored(name = "boattt.py", eTag = "eTag", created = Date.from(Instant.now()).time,
                modified = Date.from(Instant.now()).time, bucket = bucket)
        objectStoredRepository.save(obj3)
        bucket.objectsStrored.add(obj3)
        File("../buckets/${bucket.bucketName}/ggg.py").createNewFile()
        File("../buckets/${bucket.bucketName}/app.py").createNewFile()
        File("../buckets/${bucket.bucketName}/boattt.py").createNewFile()

    }

    @Transactional
    fun getBucket(bucketName: String): Bucket? {
        return bucketRepository.findByBucketName(bucketName)
    }



}