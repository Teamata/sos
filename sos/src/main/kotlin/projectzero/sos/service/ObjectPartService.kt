package projectzero.sos.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.DigestUtils
import projectzero.sos.dto.ObjectStoredUploadedDto
import projectzero.sos.entity.Bucket
import projectzero.sos.entity.ObjectPart
import projectzero.sos.entity.ObjectStored
import projectzero.sos.error.ObjectPartError
import projectzero.sos.error.RequestError
import projectzero.sos.repository.BucketRepository
import projectzero.sos.repository.ObjectPartRepository
import projectzero.sos.repository.ObjectStoredRepository
import java.io.File
import java.io.FileOutputStream
import java.lang.IllegalArgumentException
import java.net.SocketTimeoutException
import javax.servlet.ServletInputStream
import javax.servlet.http.HttpServletRequest

@Service
class ObjectPartService {

    @Autowired
    lateinit var objectPartRepository: ObjectPartRepository

    @Autowired
    lateinit var objectStoredRepository: ObjectStoredRepository

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Autowired
    lateinit var objectStoredService: ObjectStoredService

    @Transactional
    fun createFile(request: HttpServletRequest, objectStoredUploadedDto: ObjectStoredUploadedDto, bucketName: String, objectName: String, partNumber: Long, md5: String): Boolean {
        var path = ""
        try {
            var data: ServletInputStream = request.inputStream
            var f: File = File(objectName)
//            println(objectName)
            var extension = f.extension
//            println(extension)
            var name = f.nameWithoutExtension
            path = "../buckets/$bucketName/$name-$partNumber.$extension"
            var outputFile  = FileOutputStream(File(path))
            outputFile.write(request.inputStream.readBytes())
            data.close()
            outputFile.close()
        }
        catch (e : Exception) {
            when(e){
                is IllegalArgumentException -> {
                    println("content-length < body")
                }
                is SocketTimeoutException -> {
                    println("content-length > body")
                    objectStoredUploadedDto.error = RequestError.INVALIDCONTENTLENGTH.value
                }
                else -> {
                    println("weird shit")
                }
            }
            return false
        }
        if(!checkMd5(File(path), md5, objectStoredUploadedDto)){
            objectStoredUploadedDto.error = ObjectPartError.MD5MISMATCH.value
            File(path).delete()
            return false
        }
        return true
    }

    fun checkMd5(f: File, md5: String, objectStoredUploadedDto: ObjectStoredUploadedDto): Boolean {
        var stream = f.inputStream()
        var hexa = DigestUtils.md5DigestAsHex(stream)
//        println("got : $hexa")
        objectStoredUploadedDto.md5 = hexa
//        println("expected md5 : $md5")
        return hexa == md5
    }

    @Transactional
    fun savePart(objectName: String, part: Long, bucketName: String, md5: String, size: Long): Boolean {
        var objectPart: ObjectPart? = getPart(objectName,bucketName,part)
        if(objectPart != null){
            objectPart.md5 = md5
            objectPart.size = size
        }
        else{
            objectPart = ObjectPart(
                    name = objectName,
                    part = part,
                    size = size,
                    md5 = md5,
                    objectStored = objectStoredService.getObjectStored(objectName, bucketName)!!
            )
        }
        objectPartRepository.save(objectPart)
        return true
    }

    @Transactional
    fun getPart(objectName: String, bucketName: String, part: Long): ObjectPart? {
        var objectStored: ObjectStored = objectStoredService.getObjectStored(objectName, bucketName)!!
        return objectPartRepository.findByNameAndPartAndObjectStored(objectName,part, objectStored)
    }

    @Transactional
    fun deletePart(objectName: String, part: Long, bucketName: String): Boolean {
//        println("yay")
        var objectPart: ObjectPart = getPart(objectName,bucketName,part)!!
        objectPartRepository.delete(objectPart)
        var f: File = File(objectName)
        var extension = f.extension
        var name = f.nameWithoutExtension
//        println(objectName)
//        println(extension)
//        println("./buckets/$bucketName/$name-$part.$extension")
        File("../buckets/$bucketName/$name-$part.$extension").delete()
        return true
    }

}
