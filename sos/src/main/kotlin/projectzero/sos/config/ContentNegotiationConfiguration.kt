package projectzero.sos.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * @author scheaman
 * @date 2/9/18
 */
@Configuration
class ContentNegotiationConfiguration : WebMvcConfigurer {

    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        configurer!!.favorPathExtension(false)
    }
}