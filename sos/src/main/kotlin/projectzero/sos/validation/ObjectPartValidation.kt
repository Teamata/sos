package projectzero.sos.validation

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectzero.sos.dto.ObjectStoredUploadedDto
import projectzero.sos.error.BucketError
import projectzero.sos.error.ObjectPartError
import projectzero.sos.error.ObjectStoredError
import projectzero.sos.repository.ObjectPartRepository
import projectzero.sos.repository.ObjectStoredRepository

@Service
class ObjectPartValidation {

    @Autowired
    lateinit var objectStoredRepository: ObjectStoredRepository

    @Autowired
    lateinit var bucketValidation: BucketValidation

    @Autowired
    lateinit var objectPartRepository: ObjectPartRepository

    /**
     * error : InvalidPartNumber, InvalidObjectName, InvalidBucket
     */
    fun validUpload(objectName: String, bucketName: String, expectedMd5: String, expectedLength: String, partNumber: Long): ObjectStoredUploadedDto {
        var ret = ObjectStoredUploadedDto(md5 = expectedMd5, length = expectedLength.toLong(), partNumber = partNumber)
        //check if bucket is already exist

        var bucket = bucketValidation.checkCreate(bucketName)
        if(bucket == null){
            ret.error = BucketError.NOTFOUND.value
            return ret
        }
        //check if object is created
        var objStored = objectStoredRepository.findByNameAndBucket(objectName,bucket)
        if(objStored == null){
            ret.error = ObjectStoredError.INVALIDNAME.value
            return ret
        }
        if(!objStored.uploadTicket){
            ret.error = ObjectStoredError.INVALIDUPLOADTICKET.value
            return ret
        }
        //check if object flag for download is set
        if(partNumber < 1 || partNumber > 10000){
            ret.error = ObjectPartError.INVALIDNUMBER.value
            return ret
        }
        return ret
    }
}