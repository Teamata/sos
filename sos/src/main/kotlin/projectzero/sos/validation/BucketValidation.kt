package projectzero.sos.validation

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectzero.sos.entity.Bucket
import projectzero.sos.repository.BucketRepository
import java.util.regex.Pattern

@Service
class BucketValidation {

    @Autowired
    lateinit var bucketRepository: BucketRepository

    private final var NAME = Pattern.compile("^[A-Za-z0-9_\\-]+\$")


    fun checkProperName(name: String):Boolean {
        if(name.length > 1){
            return NAME.matcher(name).find()
        }
        return false
    }

    fun checkCreate(name: String): Bucket? {
        return bucketRepository.findByBucketName(name)
    }

}