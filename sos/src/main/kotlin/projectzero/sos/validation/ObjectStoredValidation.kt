package projectzero.sos.validation

import org.springframework.stereotype.Service
import java.util.regex.Pattern

@Service
class ObjectStoredValidation {

    private final var CHECKDOT = Pattern.compile("^(?![.])(?!.*[.]\$).+")

    private final var NAME = Pattern.compile("^[A-Za-z0-9_.\\-]+\$")

    private final var RANGE = Pattern.compile("(-?\\d*)-(-?\\d*)")

    fun checkProperName(name: String):Boolean {
        if(name.length > 1){
            if(CHECKDOT.matcher(name).find()){
                return NAME.matcher(name).find()
            }
        }
        return false
    }

    fun checkRange(range : String):Boolean {
        var strip = RANGE.matcher(range)
        var str = ""
        while(strip.find()){
//            println("stripped  ${strip.group()}")
            str = strip.group()
        }
//        println(str)
        var stripRange = str.split('-')
        var count = 0
        for(i in stripRange.indices){
            if(i==0 && stripRange[i].isEmpty()){
                return false
            }
            if(!stripRange[i].isEmpty()){
                count += 1
            }
        }
//        println("count : $count")
//        println("stripRange size : ${stripRange.size}")
        if( count < 1 || count > 3 || stripRange.size != 2){
            return false
        }
        var start: Long = stripRange[0].toLong()
        var end = -1L
        if(count == 2 && stripRange.size == 2) {
            end = stripRange[1].toLong()
        }
//        println("start : $start , end : $end")
        if(start < 0 || (start > end && end != -1L)){
            return false
        }
        return true
    }


}