package projectzero.sos.response

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper


data class Response(val status: String, val error: String) {

    override fun toString(): String {
        val mapper = ObjectMapper()
        try {
            return mapper.writeValueAsString(this)
        } catch (e: JsonProcessingException) {
            return null.toString()
        }

    }
}