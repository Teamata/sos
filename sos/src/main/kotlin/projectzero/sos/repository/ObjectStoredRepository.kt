package projectzero.sos.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import projectzero.sos.entity.Bucket
import projectzero.sos.entity.ObjectStored

@Repository
interface ObjectStoredRepository: JpaRepository<ObjectStored, Long> {

    fun findByNameAndBucket(name: String, bucket: Bucket): ObjectStored?
}