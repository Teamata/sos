package projectzero.sos.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import projectzero.sos.entity.ObjectPart
import projectzero.sos.entity.ObjectStored

@Repository
interface ObjectPartRepository: JpaRepository<ObjectPart, Long> {

    fun findByNameAndPartAndObjectStored(name: String, part: Long, objectStored: ObjectStored): ObjectPart?

    fun findByNameAndObjectStoredAndStartByteLessThanEqual(
            name: String,objectStored: ObjectStored,startByte: Long): List<ObjectPart>

    fun findByNameAndObjectStoredAndStartByteLessThanEqualAndEndByteGreaterThanEqual(
            name: String,objectStored: ObjectStored,startByte: Long, endByte: Long): List<ObjectPart>
}