package projectzero.sos.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import projectzero.sos.entity.Bucket

@Repository
interface BucketRepository: JpaRepository<Bucket, Long> {

    fun findByBucketName(bucketName: String): Bucket?
}