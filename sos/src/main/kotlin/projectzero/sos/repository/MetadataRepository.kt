package projectzero.sos.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import projectzero.sos.entity.Metadata

@Repository
interface MetadataRepository: JpaRepository<Metadata, Long> {
}