package projectzero.sos

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity.status
import org.springframework.mock.web.MockServletContext
import org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath
import org.springframework.test.web.servlet.RequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.view
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.web.bind.annotation.ResponseStatus
import sun.security.provider.certpath.OCSPResponse


@RunWith(SpringRunner::class)
@SpringBootTest
@WebAppConfiguration
class SosApplicationTests {

    @Autowired
    lateinit var wac: WebApplicationContext

    private var mockMvc: MockMvc? = null
    @Before
    @Throws(Exception::class)
    fun setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build()
    }

	@Test
	fun contextLoads() {
	}

    //check on running
    @Test
    fun checkApplicationRunning() {
        val servletContext = wac.servletContext
        Assert.assertNotNull(servletContext)
        Assert.assertTrue(servletContext is MockServletContext)
        Assert.assertNotNull(wac.getBean("helloController"))
    }

    @Test
    fun createDuplicateBucket() {
        val resultActions = mockMvc!!.perform(
                MockMvcRequestBuilders.post("/bucketname")
                        .param("create",""))

        val resultActions2 = mockMvc!!.perform(
                MockMvcRequestBuilders.post("/bucketname")
                        .param("create",""))
        Assert.assertNotNull(resultActions.andDo(MockMvcResultHandlers.print()))
        Assert.assertEquals(400, resultActions2.andReturn().response.status)
    }

    @Test
    fun createBucket() {
        val resultActions = mockMvc!!.perform(
                MockMvcRequestBuilders.post("/bucketname")
                        .param("create",""))
        val resultActions2 = mockMvc!!.perform(
                MockMvcRequestBuilders.delete("/bucketname")
                        .param("delete",""))
        //just to make sure we don't get duplicate

        val resultAction3 = mockMvc!!.perform(
                MockMvcRequestBuilders.post("/bucketname")
                        .param("create",""))
        Assert.assertNotNull(resultActions.andDo(MockMvcResultHandlers.print()))
        Assert.assertEquals(200, resultActions2.andReturn().response.status)
//        Assert.assertEquals("bucketname", resultActions2.andReturn().response.contentAsString)
    }
//

}
