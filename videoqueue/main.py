import os
import json
import redis
import logging
import requests
from flask import Flask, jsonify, request
from flask_cors import CORS

SOS_HOST = "http://"+os.getenv("SOS_HOST", "127.0.0.1")
SOS_PORT = os.getenv("SOS_PORT", 8080)
BASE_URL = f"{SOS_HOST}:{SOS_PORT}"
STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']
LOG = logging

app = Flask(__name__)
cors = CORS(app)

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:videothumbnaling'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

"""
    request: 
        {bucketName: String,
            objectName: String,
            targetName: String}
    on succuess:
        {status: ok,
            error: none}
    on failure:
        {status: error,
            error: bucket not found in request | 
            object not found in request | 
            target name not found in request |
            InvalidBucket | 
            PartNotFound }
"""
@app.route('/job/video/create', methods=['POST'])
def create_thumbnail_for_one():
    # JOB_NAME = 'job:create_thumbnail_for_one'
    body = request.json
    json_packed = json.dumps(body)
    bucket_name, object_name, target_name = None, None, None
    task = None 
    try:
        task = json.loads(json_packed)
    except Exception:
        LOG.exception('json.loads failed')
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    target_name = task.get('targetName')
    LOG.info(f'{bucket_name},{object_name},{target_name}')
    #check if these parameters exist in the json
    if not bucket_name:
        return jsonify({'status': 'error', 'error': 'bucket not found in request'}), 400
    if not object_name:
        return jsonify({'status': 'error', 'error': 'object not found in request'}), 400
    if not target_name:
        return jsonify({'status': 'error', 'error': 'target name not found in request'}), 400
    #check if the buckets, and objects exist in the data base
    LOG.info(f'{BASE_URL}/{bucket_name}/{object_name}/check')
    resp = requests.get(f'{BASE_URL}/{bucket_name}/{object_name}/check')
    LOG.info(f'{resp.json()}')
    if resp.status_code == STATUS_OK:
        RedisResource.conn.rpush(
        RedisResource.QUEUE_NAME,
        json_packed) 
    resp.connection.close()
    return jsonify(resp.json()),resp.status_code  

"""
    request:
        {bucketName: String}
    on succuess:
        {status: ok,
            error: none}
    on failure:
        {status: error,
            error: bucket not found in request | 
            object not found in request | 
            target name not found in request |
            InvalidBucket | 
            PartNotFound}
"""

@app.route('/job/video/createAll', methods=['POST'])
def create_thumbnail_for_all():
    # JOB_NAME = 'job:create_thumbnail_for_all'
    body = request.json 
    json_packed = json.dumps(body)
    bucket_name, object_name, target_name = None, None, None
    task = None 
    try:
        task = json.loads(json_packed)
    except Exception:
        LOG.exception('json.loads failed')
    bucket_name = task.get('bucketName')
    #check if these parameters exist in the json
    if not bucket_name:
        return jsonify({'status': 'error', 'error': 'bucket not found in request'}), 400
    #check if the buckets, and objects exist in the data base
    print(f'{BASE_URL}/{bucket_name}/{object_name}?list')
    resp = requests.get(f'{BASE_URL}/{bucket_name}?list')
    if resp.status_code == STATUS_OK:
        for obj in resp.json().get('objects'):
            #check if obj is a video files.
            print(obj.get('name'))
            if obj.get('name').split(".")[-1] in ("mov","avi","mp4"):
                LOG.info(f"found {obj.get('name')} as a video file")
                # print(obj.get('name').split('.')[-1]) 
                obj_name = ''.join(obj.get('name').split(".")[:-1])
                # LOG.info(f"{obj_name}")
                # LOG.info(f"{bucket_name}")
                js = json.dumps({'bucketName': bucket_name,
                    'objectName': obj.get('name'),
                    'targetName': obj_name+".gif"})
                LOG.info(f"{js}")
                RedisResource.conn.rpush(
                    RedisResource.QUEUE_NAME,
                    js) 
    resp.connection.close()
    return jsonify(resp.json()),resp.status_code  



