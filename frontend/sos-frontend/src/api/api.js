import axios from 'axios'

const api = axios.create({
    baseUrl: "localhost:5000",
    timeout: 10000,

});

export function getOneJob(bucketName,objectName,targetName){
    return axios.post('http://localhost:5000/job/video/create',{
        'bucketName': bucketName,
        'objectName': objectName,
        'targetName': targetName
    })
}

export function getAllJob(bucketName){
    return axios.post('http://localhost:5000/job/video/createAll',{
        'bucketName': bucketName
    })
}

export function getBucketList(bucketName){
    return axios.get(`http://localhost:8080/${bucketName}?list`,{

    })
}

export function deleteGifFile(bucketName, objectName){
    return axios.delete(`http://localhost:8080/${bucketName}/${objectName}.gif?delete`,
        {
        })
}