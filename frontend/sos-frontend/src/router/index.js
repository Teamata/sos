import Vue from 'vue'
import Router from 'vue-router'

import Landing from '@/components/Landing'

import GifDisplay from '@/components/GifDisplay'


Vue.use(Router)


export default new Router({
    routes: [
        {
            path: '/',
            name: 'Landing',
            component: Landing,
        },
        {
            path: '/signin',
            name: 'GifDisplay',
            component: GifDisplay
        },
        {
            path: '/signup',
            name: 'Signup',
            component: Signup
        }
        // {
        //   path: '*',
        //   name: 'NotFound',
        //   component: NotFound
        // }
    ],
    mode: 'history'
})
