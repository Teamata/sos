#!/usr/bin/env python3
import os
import logging
import json
import uuid
import redis
import requests
import hashlib
from moviepy.editor import VideoFileClip
import sys


SOS_HOST = "http://"+os.getenv("SOS_HOST", "127.0.0.1")
SOS_PORT = os.getenv("SOS_PORT", 8080)
BASE_URL = f"{SOS_HOST}:{SOS_PORT}"
STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']
LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:videothumbnaling'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                LOG.info("successfully get in worker")
                LOG.info(task)
                callback_func(task)

"""
    incoming json:
    {bucketName: String,
        objectName: String,
        targetName: String}
"""
def thumbnailer(log, task):
    # log.info("IN THUMBNAILER")
    clean = True 
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    target_name = task.get('targetName')
    log.info(f"data : {bucket_name}, {object_name}, {target_name}")
    headers = {'Range': 'Bytes=0-'}
    log.info(f'{BASE_URL}/{bucket_name}/{object_name}')
    download = requests.get(f'{BASE_URL}/{bucket_name}/{object_name}', headers=headers, stream=True)
    if download.status_code != STATUS_OK:
        log.info(f"something went wrong[download], status code: {download.status_code}")
        return 

    #try to open the file 

    # video_md5 = hashlib.md5()
    with open(f"./{object_name}", 'wb') as f:
        for chunk in download.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                # video_md.update(chunk)
    
    # video_md5 = video_md5.hexdigest()

    log.info(f"download * -------------- ")
    log.info(f"{download.status_code}")
    log.info(f"{download.headers}")
    log.info(f"----------------------- ")


    #check MD5, but how do we get eTag from client side?
    # if(video_md5 == download.json()

    os.system(f"./make_thumbnail {object_name} {target_name}")
    os.remove(f"./{object_name}")
    
    gif_md5 = hashlib.md5()

    with open(f"./{target_name}","rb") as gif_file:
        for chunk in iter(lambda: gif_file.read(1024), b""):
            gif_md5.update(chunk)

    gif_md5 = gif_md5.hexdigest()

    #create objecticket
    ticket = requests.post(f'{BASE_URL}/{bucket_name}/{target_name}?create')
    if ticket.status_code != STATUS_OK:
        log.info(f"something went wrong[creating ticket], status code: {ticket.status_code}")
        return 

    log.info(f"create ticket * -------------- ")
    log.info(f"{ticket.status_code}")
    log.info(f"{ticket.headers}")
    log.info(f"------------------------------ ")

    #upload parts
    log.info(f"MD5: {gif_md5}")
    headers = {'content-md5': gif_md5}
    up = requests.put(f'{BASE_URL}/{bucket_name}/{target_name}?partNumber=1', data=open(f"./{target_name}","rb"), headers=headers)
    log.info(f"upload * -------------- ")
    log.info(f"{up.status_code}")
    log.info(f"{up.headers}")
    log.info(f"----------------------- ")
    if up.status_code != STATUS_OK:
        log.info(f"somthing went wrong[uploading], status code: {up.status_code}")
        return 

    os.remove(f"./{target_name}")

    #complete
    complete = requests.post(f'{BASE_URL}/{bucket_name}/{target_name}?complete')
    if complete.status_code != STATUS_OK:
        log.info(f"something went wrong[complte upload], status code: {complete.status_code}")
        return 

    #done and clean up   
    ticket.connection.close()
    download.connection.close()
    complete.connection.close()
    up.connection.close()
    gif_file.close()
    f.close()


def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        QUEUE_NAME, 
        lambda task_descr: thumbnailer(named_logging, task_descr))

if __name__ == '__main__':
    main()
